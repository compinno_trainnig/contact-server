package com.contact.utility;

public enum EmailTemplate {
	
	USER_WELCOME_EMAIL ("userWelcomEmail.vm", "Welcome to Comprinno");
	
	private String templateName;
	private String subject;
	
	private EmailTemplate(String templateName, String subject) {
		this.templateName = templateName;
		this.subject = subject;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
	
	
}
