package com.contact.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;


/**
 * A class providing utility information
 * 
 * @author White Cube Solutions.
 *
 */
public class Utility {

	public static String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()]).{8,20})";

	 private static PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();

	/**
	 * Utility method to check if user is approved.
	 * 
	 * @param userStatus
	 * @return
	 */


	/**
	 * Utility method to check password validity based on pattern.
	 * 
	 * @param password
	 * @return
	 */
	public static boolean isPasswordValid(String password) {
		if (Pattern.compile(PASSWORD_PATTERN).matcher(password).matches()) {
			return true;
		}
		return false;
	}

	/**
	 * Method to copy data into file.
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

	/**
	 * Method to validate email
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isEmailValid(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	/**
	 * Method to validate phone
	 * 
	 * @param phone
	 * @return
	 */
	
	public static boolean isPhoneValid(String phone) {
		try {
			if (mPhoneNumberUtil.isValidNumber(mPhoneNumberUtil.parse(phone, null))) {
				return true;
			}
		} catch (NumberParseException e) {
			// e.printStackTrace();
			// Do nothing will eventually return false
		}
		return false;
	}
	 

	/**
	 * Is string phone or email.
	 * 
	 * @param source
	 * @return
	 */
	
	public static boolean isValidEmailOrPhone(String source) {
		if (isEmailValid(source)) {
			return true;
		}
		if (isPhoneValid(source)) {
			return true;
		}
		return false;
	}
	 

	/**
	 * Get phone dto from string.
	 * 
	 * @param phone
	 * @return
	 */
	
	
	public static String getJSONStringFromObj(Object obj){
		Gson gson = ObjectFactory.createGsonObject();
		String json = gson.toJson(obj);
		
		return json;
	}
	
	public static boolean isDateForToday(Date date){
		
		if(date == null){
			return false;
		}
		Calendar c = Calendar.getInstance();
	    c.set(Calendar.HOUR_OF_DAY, 0);
	    c.set(Calendar.MINUTE, 0);
	    c.set(Calendar.SECOND, 0);
	    c.set(Calendar.MILLISECOND, 0);
	    
	    Date todayStart = c.getTime();
	    if(todayStart.before(date)){
	    	return true;
	    }
	    
	    return false;
	}
	
	public static Integer getTotalDays(Date startDate, Date endDate){
		if(startDate != null && endDate != null){
			long diff = endDate.getTime() - startDate.getTime();
			long diffDays =  diff / (1000 * 60 * 60 * 24);
			return Integer.valueOf((int)diffDays + 1);
		}
		return null;
	}
}