package com.contact.utility;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utility class use to map message codes to messages.
 * 
 * @author Niraj Ranjan
 *
 */
public class MessageDetailsHelper {

	private static final Logger logger = LogManager.getLogger(MessageDetailsHelper.class);

	// Org Error codes
	public static final String ORG_NF_FOR_CODE = "ORG_NF_001";
	
	// Generic Error code
	public static final String GENERAL_SERVER_ERR = "GEN_ERR_001";
	
	// Validation Error codes
	public static final String VAL_INVALID_DATA_GENERIC = "VAL_INV_001";
	public static final String VAL_DATA_REQUIRED = "VAL_DATA_REQ";
	public static final String VAL_INVALID_DATA = "VAL_INV_DATA";
	
	// User error code
	public static final String USER_NOT_FOUND = "USER_NF_001";
	public static final String USER_NAME_REQ = "USER_NAME_REQ";
	public static final String USER_PASSWORD_REQ = "USER_PASSWORD_REQ";
	public static final String LOGIN_NOT_RECOGNIZED = "LOGIN_NOT_RECOGNIZED";
	
	public static final String INVALID_LOGIN_OR_PASSWORD = "INVALID_LOGIN_OR_PASSWORD";
	public static final String USER_NOT_APPROVED = "USER_NOT_APPROVED";
	public static final String USER_NOT_VERIFIED = "USER_NOT_VERIFIED";
	public static final String USER_NOT_ACTIVE = "USER_NOT_ACTIVE";
	public static final String USER_UNIT_NOT_ASSOCIATED = "USER_UNIT_NOT_ASSOCIATED";
	public static final String ACCESS_NOT_ALLOWED = "ACCESS_NOT_ALLOWED";
	public static final String USER_ROLES_CONFLICT = "USER_ROLES_CONFLICT";
	
	public static final String USER_INFO_NOT_AVL = "USER_INFO_NOT_AVL";
	
	public static final String UNABLE_TO_SEND_MESSAGE= "UNABLE_TO_SEND_MESSAGE";
	public static final String OTP_INVALID = "OTP_INVALID";
	public static final String OTP_EXPIRED = "OTP_EXPIRED";
	public static final String OTP_MAX_ATTEMPT = "OTP_MAX_ATTEMPT";
	public static final String PASSWORD_PATTERN_INVALID = "PASSWORD_PATTERN_INVALID";
	
	
	// Org Unit error codes
	public static final String UNIT_NOT_FOUND = "UNIT_NF_001";
	
	// Visitor error codes
	public static final String VISITOR_NOT_FOUND = "VISIT_NF_001";
	
	public static final String VISITOR_TYPE_NOT_FOUND = "VISIT_TYPE_NF_001";
	
	// Image error codes
	public static final String ENTITY_NOT_FOUND = "ENTITY_NF_001";
	public static final String ENTITY_ALREADY_EXISTS = "ENTITY_ALREADY_EXISTS";

	// License error codes
	public static final String LICENSE_NOT_ACTIVE = "LICENSE_NOT_ACTIVE";
	
	//public static final String CUSTOM_MESSAGE = "CUSTOM_MESSAGE";
	public static final String CONFIG_MAX_PHONE="CONFIG_MAX_PHONE";
	
	public static final String NO_ACTIVE_USER_FOR_CALL = "NO_ACTIVE_USER_FOR_CALL";
	public static final String USERS_DND_SET = "USERS_DND_SET";
	
	public static final String START_DATE_PAST = "START_DATE_PAST";
	public static final String END_DATE_PAST = "END_DATE_PAST";
	public static final String END_DATE_BEFORE_START = "END_DATE_BEFORE_START";
	
	public static final String SMS_MSG_FOR_OTP = "SMS_MESSAGE_FOR_OTP";
	public static final String SMS_MSG_FOR_USER_CRED = "SMS_MESSAGE_FOR_USER_CRED";
	public static final String SMS_MESSAGE_FOR_REG_1 = "SMS_MESSAGE_FOR_REG_1";
	public static final String SMS_MESSAGE_FOR_REG_2 = "SMS_MESSAGE_FOR_REG_2";
	
	public static final String OAUTH_INVALID_REFRESH_TOKEN = "OAUTH_INVALID_REFRESH_TOKEN";
	
	// Auth errors
	public static final String AUTH_INVALID_CLIENT = "AUTH_001";
	public static final String AUTH_AUTH_HEADER_REQUIRED = "AUTH_002";
	public static final String AUTH_INVALID_AUTH_TOKEN = "AUTH_003";
	public static final String ACCESS_DENIED = "AUTH_004";
	public static final String AUTH_ORG_EXPIRED = "AUTH_005";
	
	public static final String EMAIL_USER_ADD = "EMAIL_USER_ADD";
	
	public static final String SUBS_SAME_FREE_NOT_ALLOWED = "SAME_FREE_NOT_ALLOWED";
	public static final String R_VISITOR_ALREADY_ASSIGNED = "R_VISITOR_ALREADY_ASSIGNED";
	
	public static final String SMS_MESSAGE_FOR_SOS = "SMS_MESSAGE_FOR_SOS";
	public static final String SMS_MESSAGE_FOR_VISITOR_INVIITE = "SMS_MESSAGE_FOR_VISITOR_INVIITE";
	
	private static final Properties prop = new Properties();
	
	static{
		try {
			InputStream inputStream = MessageDetailsHelper.class.getClassLoader().getResourceAsStream("messages.properties");
			prop.load(inputStream);
		} catch (Exception e) {
			logger.error("ErrorDetailsHelper, static block, Error while loading properties: ", e);
		}
		
	}
	
	/**
	 * Method to get message from code.
	 * 
	 * @param code
	 * @return
	 */
	public static String getMessage(String code){
		return prop.getProperty(code);
	}
	
	/**
	 * Method to get message for code and replace all the variables with the value provided.
	 * @param code
	 * @param variables
	 * @return
	 */
	public static String getMessage(String code, List<String> variables){
		String message = prop.getProperty(code);
		
		if(message != null && variables != null && !variables.isEmpty()){
			int index = 0;
			String replaceStr = null;
			for (String var : variables) {
				replaceStr = "{"+index+"}";
				message = message.replace(replaceStr, var);
				index++;
			}
		}
		
		return message;
	}
	
	/**
	 * Method to get message for code and replace only one variable with the value provided.
	 * @param code
	 * @param variable
	 * @return
	 */
	public static String getMessage(String code, String variable){
		ArrayList<String> variables = new ArrayList<>();
		if(variable != null){
			variables.add(variable);
		}
		
		return getMessage(code, variables);
	}
}
