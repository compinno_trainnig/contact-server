package com.contact.utility;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 * This class takes care or taking required data from client and send request to
 * server and return the response.
 * 
 * @author Niraj Ranjan
 * 
 */
public class HttpRequest {

	private static final Logger logger = LogManager.getLogger(HttpRequest.class);
	
	/** URL of request to be executed*/
	private String url;
	
	/** Request type: Whether GET, POST, PUT or DELETE*/
	private HttpRequestType requestType;
	
	/*** Content Type: XML, HTML, etc.*/
	private HttpContentType contentType;
	
	/** Data to be posted in POST and PUT request */
	private String data;
	
	/** Post parameters */
//	private List<NameValuePair> params;
	
	/*set this field if you want to send some headers in the request */
	private Map<String,String> headersMap;
	
	/*set this field if you want to send some cookie in the request */
	private Map<String,HttpCookie> cookiesMap;
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the requestType
	 */
	public HttpRequestType getRequestType() {
		if(requestType == null)
			requestType = HttpRequestType.GET;
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(HttpRequestType requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the contentType
	 */
	public HttpContentType getContentType() {
		if(contentType == null)
			contentType = HttpContentType.PLAIN_TEXT;
		
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(HttpContentType contentType) {
		this.contentType = contentType;
	}

	
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	
	/**
	 * @return the params
	 */
//	public List<NameValuePair> getParams() {
//		return params;
//	}

	/**
	 * @param params the params to set
	 */
//	public void setParams(List<NameValuePair> params) {
//		this.params = params;
//	}

	/**
	 * Constructor using URL, requestType and content type.
	 * @param url
	 * @param requestType
	 * @param contentType
	 */
	public HttpRequest(String url, HttpRequestType requestType,
			HttpContentType contentType) {
		super();
		this.url = url;
		this.requestType = requestType;
		this.contentType = contentType;
	}

	/**
	 * Constructor using URL and requestType
	 * @param url
	 * @param requestType
	 */
	public HttpRequest(String url, HttpRequestType requestType) {
		super();
		this.url = url;
		this.requestType = requestType;
	}

	/**
	 * Constructor using URL
	 * @param url
	 * @param requestType
	 */
	public HttpRequest(String url) {
		this.url = url;
	}


	/*
	 * Request Type Enum
	 */
	public enum HttpRequestType {
		GET, POST, PUT, DELETE
	}

	/*
	 * Content Type Enum
	 */
	public enum HttpContentType {
		JSON, XML, HTML, PLAIN_TEXT;
		
		public String getString(){
			if(this == JSON){
				return "application/json";
			}else if(this == XML){
				return "application/xml";
			}else if(this == HTML){
				return "text/html";
			}else if(this == PLAIN_TEXT){
				return "text/plain";
			} else{
				return "text/plain";
			}
		}
	}

	/**
	 * Method to execute the Http request based on parameters.
	 * 
	 * @return
	 */
	public HttpResponse executeRequest(){
		
		logger.info("HttpRequest, executeRequest(), starts:" );
		HttpResponse response = null;
		
		if(getRequestType() == HttpRequestType.GET){
			response = executeHttpGetRequest();
		}else if(getRequestType() == HttpRequestType.POST){
			response = executeHttpPostRequest();
		}else if(getRequestType() == HttpRequestType.PUT){
			response = executeHttpPutRequest();
		}else if(getRequestType() == HttpRequestType.DELETE){
			response = executeHttpDeleteRequest();
		}
		
		return response;
	}
	
	
	/**
	 * Method to execute POST request and return response.
	 * 
	 * @return Http Response
	 */
	private HttpResponse executeHttpPostRequest() {

		// Create HttpClient
		HttpClient client = createHttpClient();
		HttpResponse response = null;

        try {
        	// createPost object and set content type and data
        	HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", getContentType().getString());
            if(data != null)
            	post.setEntity(new StringEntity(data,"UTF-8"));
            
            /*if(params != null && !params.isEmpty()){
            	for (NameValuePair param : params) {
            		post.setParams(param);
				}
            }*/
            
           	//add headers if any
        	this.addHeadersToRequest(post);
            
            // Execute request.
        	response = client.execute(post);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	/**
	 * Method to execute Get request and return response
	 * @return Http Response
	 */
	private HttpResponse executeHttpGetRequest() {

		// Create HttpClient
		HttpClient client = createHttpClient();

		HttpResponse response = null;
        try {
        	HttpGet get = new HttpGet(url);

        	//add headers if any
        	this.addHeadersToRequest(get);
        	
        	// Execute request.
        	response = client.execute(get);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}

	
	/**
	 * Method to execute Put request and return response
	 * @return Http Response
	 */
	private HttpResponse executeHttpPutRequest() {

		// Create HttpClient
		HttpClient client = createHttpClient();
		HttpResponse response = null;

        try {
        	// createPost object and set content type and data
        	HttpPut put = new HttpPut(url);
            put.setHeader("Content-Type", contentType.getString());
            if(data != null)
            	put.setEntity(new StringEntity(data,"UTF-8"));
            
            //add headers if any
        	this.addHeadersToRequest(put);
            
			// Execute request.
        	response = client.execute(put);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	/**
	 * Method to execute DELETE request and return response
	 * @return Http Response
	 */
	private HttpResponse executeHttpDeleteRequest() {

		// Create HttpClient
		HttpClient client = createHttpClient();
		HttpResponse response = null;

        try {
        	// createPost object and set content type and data
        	HttpDelete delete = new HttpDelete(url);
        	
        	//add headers if any
        	this.addHeadersToRequest(delete);

			// Execute request.
        	response = client.execute(delete);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}


	/**
	 * This method takes care of calling HttpRequest and getting the response
	 * @param url
	 * @return
	 */
	public String getResponseTextForHttpRequest(){
		
		logger.info("HttpRequest, getResponseTextForHttpRequest(), starts:");

		HttpResponse response= executeRequest();
		
		String responseText = null;
		
		if(response != null && response.getStatusLine()!= null 
				&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
			
			try {
				responseText = EntityUtils.toString(response.getEntity());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			if(response == null){
				logger.info("HttpRequest, getResponseTextForHttpRequest(), error occured: HttpResponse is NULL");
			}else if(response.getStatusLine()!= null ){
				logger.info("HttpRequest, getResponseTextForHttpRequest(), error occured: HttpResponse.StatusCode = " + response.getStatusLine().getStatusCode());
			}
			try {
				
				if(response != null && response.getEntity() != null){
					logger.info("HttpRequest, getResponseTextForHttpRequest(), error occured: Response = " + EntityUtils.toString(response.getEntity()));
				}
			} catch (Exception e) {
				logger.error("HttpRequest, getResponseTextForHttpRequest(), error occured", e );
			}
		}
		
		logger.info("HttpRequest, getResponseTextForHttpRequest(), ends: Response = " + responseText);

		return responseText;
	}

	/**
	 * @param headersMap the headersMap to set
	 */
	public void setHeadersMap(Map<String,String> headersMap) {
		this.headersMap = headersMap;
	}

	/**
	 * @return the headersMap
	 */
	public Map<String,String> getHeadersMap() {
		return headersMap;
	}
	
	/**
	 * @return
	 */
	private void addHeadersToRequest(HttpRequestBase request){
		if(this.headersMap != null && !this.headersMap.isEmpty()){
			Set<String> keySet = headersMap.keySet();
			for(String key :keySet){
				request.addHeader(key,headersMap.get(key));
			}
		}
	}
	
	/**
	 * Create store for give cookie data.
	 * @return
	 */
	private BasicCookieStore getCookieStore(){
		BasicCookieStore cookieStore = new BasicCookieStore();
		if(this.cookiesMap != null && !this.cookiesMap.isEmpty()){
			Set<String> keySet = cookiesMap.keySet();
			for(String key :keySet){
				HttpCookie httpCookie = (HttpCookie)cookiesMap.get(key);
				BasicClientCookie cookie = new BasicClientCookie(httpCookie.getName(),httpCookie.getValue());
		        if(httpCookie.getDomain() != null){
		        	cookie.setDomain(httpCookie.getDomain());
		        }
		        if(httpCookie.getPath() != null){
		        	cookie.setPath(httpCookie.getPath());
		        }else{
		        	cookie.setPath("");
		        }
		        cookieStore.addCookie(cookie);
			}
			
		}
		
		return cookieStore;
	}

	private CloseableHttpClient createHttpClient() {
		HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setDefaultCookieStore(getCookieStore());
		
		CloseableHttpClient client = builder.build();
		return client;
	}
	
	/**
	 * @param cookiesMap the cookiesMap to set
	 */
	public void setCookiesMap(Map<String,HttpCookie> cookiesMap) {
		this.cookiesMap = cookiesMap;
	}

	/**
	 * @return the cookiesMap
	 */
	public Map<String,HttpCookie> getCookiesMap() {
		return cookiesMap;
	}
	
}
