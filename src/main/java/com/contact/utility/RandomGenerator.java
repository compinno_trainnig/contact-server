package com.contact.utility;

import java.util.Random;

/**
 * Class to give utility methods to generate Random things.
 * E.g. Random number, OTP, etc.
 * 
 * @author White Cube Solution
 *
 */
public class RandomGenerator {

	public static String alphaSeq = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static String specialCharSeq = "!@#$%^&*()"; 

	
	/**
	 * Generates six digit random number.
	 * @return
	 */
	public static Integer generateSixDigitRandomNumber(){
		/*Double d = new Double(Math.random() * 1000000);
		Integer i = d.intValue();
		if(i.toString().length() == 6){
			return i;
		} else {
			return generateSixDigitRandomNumber();
		}*/
		return generateNDigitRandomNumber(6);
		
	}
	
	/**
	 * Generate n digit random number
	 * @param n
	 * @return
	 */
	public static Integer generateNDigitRandomNumber(int n) {

		if (n == 0) {
			return null;
		}

		Double multiplier = Math.pow(10, n);

		Double d = new Double(Math.random() * multiplier);
		Integer i = d.intValue();
		if (i.toString().length() == n) {
			return i;
		} else {
			return generateNDigitRandomNumber(n);
		}

	}
	
	/**
	 * Method to generate random password for given length.
	 * Note: if length <=4 it will be all alphabet characters
	 * if length > 4 then 1 - special, 3 - numbers and remaining alphabet characters
	 * @param length
	 * @return
	 */
	public static String generateRandomPassword(int length){
		StringBuilder sb = new StringBuilder("");
		
		if(length > 0){
			int charLen = length - 4;
			if(charLen <=0){
				charLen = length;
			}
			
			Random rnd = new Random();
			for(int cnt=0; cnt<charLen; cnt++){
				sb.append(alphaSeq.charAt(rnd.nextInt(alphaSeq.length())));
			}
			
			// now if length is still pending use special char
			if(length > charLen){
				sb.append(specialCharSeq.charAt(rnd.nextInt(specialCharSeq.length())));
			}
			
			// If still remaining, add max 3 numbers
			if(length > charLen+1){
				int numLen = length - (charLen+1);
				Integer num = generateNDigitRandomNumber(numLen);
				if(num != null){
					sb.append(num.toString());
				}
			}
			
		}
		
		return sb.toString();
	}
	
	/**
	 * Method to generate 8 character password.
	 * @return
	 */
	public static String generate8CharRandomPassword(){
		return generateRandomPassword(8);
	}
	
}
