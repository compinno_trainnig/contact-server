package com.contact.utility;

public interface NamedQueryName {
	
	// Org specific Named queries
	String USER_FIND_BY_EMAIL = "findUserByEmail";
	String TEACHER_FIND_BY_EMAIL = "findTeacherByEmail";
	
	String BROADCAST_MESSAGE_FOR_SEM = "findAttachmentBySem";
	String CONTACT = "getAllContact";
	String CONTACT_BY_ID = "getContactById";
	

	
	
}
