package com.contact.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Base entity.
 * @author 
 *
 */
@MappedSuperclass
public abstract class BaseEntity {
	
	public static final String TO_STRING_SEPARATOR = ", ";
	
	@Column(name="CREATED_DATE")
//	@CreatedDate
	private Date createdDate;
	
	@Column(name="MODIFIED_DATE")
//	@LastModifiedDate
	private Date modifiedDate;

	/*@Column(name = "created_by")
    @CreatedBy
    private String createdBy;
 
    @Column(name = "modified_by")
    @LastModifiedBy
    private String modifiedBy;*/
	
	
	abstract public Integer getId();
	
	/**
	 * NOTE: Override in all audited entities
	 * 
	 * returns the old and new values after bean comparison. 
	 * Current object is the old is the object
	 *  
	 * @param newEntity - New version of the same object
	 * @return Array of 2 strings. [0] will have old value, [1] will have new value
	 */
	public String[] getDiffedEntities(BaseEntity newEntity){
		//default implementation
		return null;
	}
	
	/**
	 * NOTE: Override in all audited entities
	 * 
	 * @return all entity properties in a readable format
	 */
	public String toReadableString() {
		return toString();
	}
	
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void appendToReadableString(StringBuilder readableStr, String varName, Object value){
		if(value != null && readableStr != null){
			readableStr.append(TO_STRING_SEPARATOR).append(varName).append('=').append(value);
		}
	}
	
	public void appendReadableStrToReadableStr(StringBuilder readableStr, String varName, BaseEntity value){
		if(value != null && readableStr != null){
			readableStr.append(TO_STRING_SEPARATOR).append(varName).append(" [").append(value.toReadableString()).append(" ] ");
		}
	}
	
	public void appendDiffedString(StringBuilder oldStr, StringBuilder newStr, String varName, Object oldObj, Object newObj){
		if(!Objects.equals(oldObj, newObj)){
			appendToReadableString(oldStr, varName, oldObj);
			appendToReadableString(newStr, varName, newObj);
		}
	}
	
	public void appendDiffedEntity(StringBuilder oldStr, StringBuilder newStr, String varName, BaseEntity oldObj, BaseEntity newObj){
		if(!Objects.equals(oldObj, newObj)){
			appendReadableStrToReadableStr(oldStr, varName, oldObj);
			appendReadableStrToReadableStr(newStr, varName, newObj);
		}
	}
} 
