package com.contact.controller.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.contact.dto.BaseRestResponse;
import com.contact.dto.ContactDTO;
import com.contact.dto.MessageData;
import com.contact.exception.BaseAPIException;
import com.contact.service.ContactService;
import com.contact.utility.DataUtil;
import com.contact.dto.ResponseDTO;


@RestController
@RequestMapping("/api/contact")
public class ContactAPI extends BaseAPI{
	private static final Logger logger = LogManager.getLogger(ContactAPI.class);
	
	@Autowired
	ContactService contactService;
	
	
	
	/**
	 * API to create contact into system.
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	
	//@CrossOrigin(origins = "http://localhost:4200")
	//@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	//public ResponseEntity<BaseRestResponse> addContact( @RequestBody ContactDTO contactDtos) throws BaseAPIException{
		
		//logger.info("ContactAPI, Contact, starts with Id = {}, message = {}", contactDtos);
		
		//List<MessageData> errors = new ArrayList<>();
		
		//contactDtos = contactService.createContact(contactDtos, errors);
		
		//if(!DataUtil.isCollectionNullOrEmpty(errors)){
			//ResponseEntity<BaseRestResponse> response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			//return response;
		//}
		
		//ResponseEntity<BaseRestResponse> response = createResponseEntity(contactDtos, HttpStatus.OK);
		//return response;
	//}
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{userId}/contact", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> addContactById( @PathVariable("userId") Integer userId,@RequestBody ContactDTO contactDtos) throws BaseAPIException{
		
		logger.info("ContactAPI, Contact, starts with Id = {}, message = {}", contactDtos);
		
		List<MessageData> errors = new ArrayList<>();
		
		contactDtos = contactService.createContact(userId,contactDtos, errors);
		
		if(!DataUtil.isCollectionNullOrEmpty(errors)){
			ResponseEntity<BaseRestResponse> response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			return response;
		}
		
		ResponseEntity<BaseRestResponse> response = createResponseEntity(contactDtos, HttpStatus.OK);
		return response;
	}
	
	
	
	
	
	/**
	 * API to get All Contact
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/all/{userId}", method=RequestMethod.GET)
	public ResponseEntity<BaseRestResponse> getbroadcastMessage(@PathVariable("userId") Integer userId) throws BaseAPIException, Exception{
		
		logger.info("ContactAPI, Contact, starts with Id = {}, message = {}");
		
		List<MessageData> errors = new ArrayList<>();
		List<ContactDTO> contactDTO = contactService.getAllContact(userId,errors);
		
		if(!DataUtil.isCollectionNullOrEmpty(errors)){
			ResponseEntity<BaseRestResponse> response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			return response;
		}
		
		ResponseEntity<BaseRestResponse> response = createResponseEntity(contactDTO, HttpStatus.OK);
		return response;
	}
	
	
	
	/**
	 * API to get contact By Id.
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{contactId}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> getBroadcastMessageById(@PathVariable("contactId") Integer contactId) throws BaseAPIException, Exception{
		
		logger.info("ContactAPI, Contact, starts." +contactId);
		ContactDTO broadcastMessageDTO = contactService.getMessageById(contactId);
		ResponseEntity<BaseRestResponse> response = createResponseEntity(broadcastMessageDTO, HttpStatus.OK);
		return response;
	}
	
	/**
	 * API to get delete contact using conatctId.
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{contactId}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> deleteContactById(@PathVariable("contactId") Integer contactId) throws BaseAPIException{
		logger.info("ContactAPI, Contact, starts for ID = " + contactId);
		
		List<MessageData> errors = new ArrayList<>();
		boolean success = contactService.deleteContact(contactId, errors);
		
		if(!errors.isEmpty()){
			ResponseEntity<BaseRestResponse> errorResponse =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			return errorResponse;
		}
		ResponseDTO res = new ResponseDTO();
		res.setSuccess(success);
		
		ResponseEntity<BaseRestResponse> response =  createResponseEntity(res, HttpStatus.OK);
		logger.info("BroadcastMessageAPI, deleteBroadcastMessage, ends");
		return response;
	}
	
	/**
	 * API  to update the Contact data into database.
	 * 
	 * @param contactDTO
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{contactId}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> updateMessageById(@RequestBody ContactDTO contactDTO , @PathVariable("contactId") Integer contactId) throws BaseAPIException{
		
		logger.info("ContactAPI, updateContactAPI, starts with id = " + contactId);
		
		List<MessageData> errors = new ArrayList<>();
		contactDTO = contactService.updateBroadcastMessage(contactId, contactDTO, errors);
		
		if(!DataUtil.isCollectionNullOrEmpty(errors)){
			ResponseEntity<BaseRestResponse> response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			return response;
		}
		
		
		ResponseEntity<BaseRestResponse> response =  createResponseEntity(contactDTO, HttpStatus.OK);
		
		logger.info("ContactAPI, updateContactAPI, ends. Id = " + contactDTO.getId());
		return response;
	}
	
	
	
	/**
	 * API to delete All contact
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/deleteall", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> deleteAllUser() throws BaseAPIException{
		logger.info("ContactAPI, updateContactAPI, starts, User Id = {}, Data = ");

		List<MessageData> errors = new ArrayList<>();
		
		boolean sucess= contactService.deletaAllContactDetails(errors);
			if(!DataUtil.isCollectionNullOrEmpty(errors)){
				ResponseEntity<BaseRestResponse> response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
				return response;
			}
		
			ResponseDTO res = new ResponseDTO();
			res.setSuccess(sucess);
			
			ResponseEntity<BaseRestResponse> response =  createResponseEntity(res, HttpStatus.OK);
			logger.info("ContactAPI, deleteContactAPI, ends");
			return response;
		
	}
	
	

}
