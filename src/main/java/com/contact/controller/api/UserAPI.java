package com.contact.controller.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.contact.dto.BaseRestResponse;
import com.contact.dto.LoginRequest;
import com.contact.dto.MessageData;
import com.contact.dto.UserDTO;
import com.contact.entity.User;
import com.contact.exception.BaseAPIException;
import com.contact.service.UserService;
import com.contact.utility.DataUtil;
import com.contact.validator.UserDataValidator;



@RestController
@RequestMapping("/api/users")
public class UserAPI extends BaseAPI{

	private static final Logger logger = LogManager.getLogger(UserAPI.class);
	
	@Autowired
	UserService userService;
	

	
	/**
	 * API to create user into system.
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> createUser(@RequestBody UserDTO userdto) throws BaseAPIException{
		logger.info("UserAPI, createUser, starts, User data = "+userdto);
		
		List<MessageData> errors = new ArrayList<>();
		
		userdto = userService.createUser(userdto, errors);
		
		
		//boolean valid = user.getValidData() != null && user.getValidData();
		//if(!valid && !DataUtil.isCollectionNullOrEmpty(errors)){
			//ResponseEntity<BaseRestResponse> errorResponse =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			//return errorResponse;
		//}
		
		ResponseEntity<BaseRestResponse> response =  createResponseEntity(userdto, errors, HttpStatus.OK);
		
		logger.info("UserAPI, createUser, ends, User Id = "+userdto.getId());
		
		return response;
	}
	
	/**
	 * API to login user
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/login", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> login(@RequestBody LoginRequest request){
		logger.info("UserAPI, login, starts, req = {}" , request);
		List<MessageData> errors = null;
		ResponseEntity<BaseRestResponse> response =  null;
		
		//if(!DataUtil.isCollectionNullOrEmpty(errors)){
			//response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
			//return response;
		//}
		UserDTO data = userService.validateLogin(request, null);
			if(data == null){
				// Invalid credentials
			response =  createResponseEntity(errors, HttpStatus.FORBIDDEN);
			return response;
		} else {
					response =  createResponseEntity(data, errors, HttpStatus.OK);
					return response;
			
			}
	}
	

	/**
	 * API to reset user password into system.
	 * 
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{userId}/resetPwd", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> resetUserPassword(@PathVariable("userId") Integer userId, @RequestBody User user) throws BaseAPIException{
		logger.info("UserAPI, resetUserPassword, starts, User Id = {}, Data = ", userId, user);

		List<MessageData> errors = new ArrayList<>();
		ResponseEntity<BaseRestResponse> response =  null;
		
		if(user != null){
			//user = userService.updateUserDetails(userId, user, errors);
		}
		response =  createResponseEntity(user, errors, HttpStatus.OK);
		
		logger.info("UserAPI, resetUserPassword, ends, User Data = "+user);
		
		return response;
		
	}
	
	/**
	 * API to update user data. Expecting only data that needs updated. 
	 * @param userId
	 * @param user
	 * @return
	 * @throws BaseAPIException
	 */
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value="/{userId}", method=RequestMethod.PUT, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseRestResponse> updateUser(@PathVariable("userId") Integer userId, @RequestBody UserDTO userdto) throws BaseAPIException{
		logger.info("UserAPI, updateUser, starts, User Id = {}, Data = ", userId, userdto);

		List<MessageData> errors = new ArrayList<>();
		ResponseEntity<BaseRestResponse> response =  null;
		
		if(userdto != null){
			userdto = userService.updateUserDetails(userId, userdto, errors);
			if(!DataUtil.isCollectionNullOrEmpty(errors)){
				response =  createResponseEntity(errors, HttpStatus.BAD_REQUEST);
				return response;
			}
		}
		response =  createResponseEntity(userdto, errors, HttpStatus.OK);
		
		logger.info("UserAPI, updateUser, ends, User Data = "+userdto);
		
		return response;
		
	}
	
	

	
	
	
}



