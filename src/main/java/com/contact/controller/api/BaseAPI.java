package com.contact.controller.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.contact.dto.BaseRestResponse;
import com.contact.dto.MessageData;
import com.contact.dto.MessageType;
import com.contact.utility.DataUtil;





/**
 * Base class for all APIs.
 * 
 *
 */
public class BaseAPI {
	

	public ResponseEntity<BaseRestResponse> createResponseEntity(Object data, HttpStatus status){
		BaseRestResponse restRes = new BaseRestResponse();
		restRes.setData(data);
		ResponseEntity<BaseRestResponse> responseEntity =  new ResponseEntity<BaseRestResponse>(restRes, status);
		
		return responseEntity;
	}
	
	public ResponseEntity<BaseRestResponse> createResponseEntity(List<MessageData> messages, HttpStatus status){
		BaseRestResponse restRes = new BaseRestResponse();
		restRes.setMessages(messages);
		ResponseEntity<BaseRestResponse> responseEntity =  new ResponseEntity<BaseRestResponse>(restRes, status);
		
		return responseEntity;
	}
	
	public ResponseEntity<BaseRestResponse> createResponseEntity(Object data, List<MessageData> messages, HttpStatus status){
		BaseRestResponse restRes = new BaseRestResponse();
		restRes.setMessages(messages);
		restRes.setData(data);
		ResponseEntity<BaseRestResponse> responseEntity =  new ResponseEntity<BaseRestResponse>(restRes, status);
		
		return responseEntity;
	}
	
	//public ResponseEntity<BaseRestResponse> createResponseEntity(Object data, OAuthAccessTokenDTO accessDto, HttpStatus status){
		//BaseRestResponse restRes = new BaseRestResponse();
		//restRes.setData(data);
		//restRes.setAccessInfo(accessDto);
		//ResponseEntity<BaseRestResponse> responseEntity =  new ResponseEntity<BaseRestResponse>(restRes, status);
		
	//	return responseEntity;
//	}
	
	/**
	 * Automatically sets response as OK or BAD_REQUEST based on errors.
	 * 
	 * @param data
	 * @param messages
	 * @return
	 */
	public ResponseEntity<BaseRestResponse> createResponseEntity(Object data, List<MessageData> messages){
		BaseRestResponse restRes = new BaseRestResponse();
		restRes.setMessages(messages);
		restRes.setData(data);
		HttpStatus status = HttpStatus.OK;
		if(hasError(messages)){
			status = HttpStatus.BAD_REQUEST;
		}
		ResponseEntity<BaseRestResponse> responseEntity =  new ResponseEntity<BaseRestResponse>(restRes, status);
		
		return responseEntity;
	}

	private boolean hasError(List<MessageData> messages) {
		boolean hasError = false;
		
		if(!DataUtil.isCollectionNullOrEmpty(messages)){
			for (MessageData messageData : messages) {
				if(messageData != null && messageData.getType() != null && messageData.getType() == MessageType.Error){
					hasError = true;
				}
			}
		}
		
		return hasError;
	}

	

	

}
