package com.contact.validator;

import java.util.ArrayList;
import java.util.List;

import com.contact.dto.MessageData;
import com.contact.dto.MessageType;
import com.contact.utility.DataUtil;
import com.contact.utility.MessageDetailsHelper;



/**
 * Base class for validators. This will have basic methods required by all validators.
 * 
 * @author Vinit
 *
 */
public class BaseValidator {

	/**
	 * Method to add error code / message to error's list.
	 * 
	 * @param errorCode
	 * @param errorMessage
	 * @param errors
	 */
	protected void addErrorToList(String errorCode, String errorMessage, List<MessageData> errors){
		MessageData error = new MessageData(errorCode, errorMessage, MessageType.Error);
		errors.add(error);
	}
	
	/**
	 * Method to add error code / message to error's list.
	 * 
	 * @param errorCode
	 * @param errorMessage
	 * @param errors
	 */
	protected void addErrorToList(String errorCode, List<MessageData> errors, List<String> messageVariables){
		String message = MessageDetailsHelper.getMessage(errorCode, messageVariables);
		MessageData error = new MessageData(errorCode, message, MessageType.Error);
		errors.add(error);
	}
	
	/**
	 * Method to add error code / message to error's list.
	 * 
	 * @param errorCode
	 * @param errorMessage
	 * @param errors
	 */
	protected void addErrorToList(String errorCode, List<MessageData> errors, String messageVariable){
		String message = MessageDetailsHelper.getMessage(errorCode, messageVariable);
		MessageData error = new MessageData(errorCode, message, MessageType.Error);
		errors.add(error);
	}
	
	/**
	 * Methods checks the required field and if not available, adds error code to the error list.
	 * @param obj
	 * @param errorCode
	 * @param errors
	 */
	public void required(Object obj, String errorCode, List<MessageData> errors, List<String> messageVariables){
		if(DataUtil.isObjectNullOrEmpty(obj)){
			addErrorToList(errorCode, errors, messageVariables);
		}
	}
	
	/**
	 * Methods checks the required field and if not available, adds error code to the error list.
	 * @param obj
	 * @param errorCode
	 * @param errors
	 */
	public void required(Object obj, String errorCode, List<MessageData> errors, String messageVariable){
		List<String> messageVariables = null;
		if(DataUtil.isStringNotEmpty(messageVariable)){
			messageVariables = new ArrayList<>();
			messageVariables.add(messageVariable);
		}
		
		required(obj, errorCode, errors, messageVariables);
	}
}
