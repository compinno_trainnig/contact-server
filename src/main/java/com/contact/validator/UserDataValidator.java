package com.contact.validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.contact.dao.UserDAO;
import com.contact.dto.MessageData;
import com.contact.dto.UserDTO;
import com.contact.entity.User;
import com.contact.exception.BaseAPIException;
import com.contact.exception.DataNotFoundException;
import com.contact.exception.InvalidDataException;
import com.contact.utility.MessageDetailsHelper;



@Configuration
public class UserDataValidator extends BaseValidator{
	

	private static final Logger logger = LogManager.getLogger(UserDataValidator.class);


	
	@Autowired
	UserDAO userDao;

	public List<MessageData> validateCreateUserRequest(UserDTO userData){		
		List<MessageData> errors = new ArrayList<>();
		
		if(userData == null){
			addErrorToList(MessageDetailsHelper.VAL_INVALID_DATA_GENERIC, null, errors);
		} else {
			
			boolean duplicateFound = duplicateEmailExists(userData.getEmail(), null);
			if(duplicateFound){
				String errorMessage = MessageDetailsHelper.getMessage(MessageDetailsHelper.ENTITY_ALREADY_EXISTS, "Email Address");
				addErrorToList(MessageDetailsHelper.ENTITY_ALREADY_EXISTS, errorMessage, errors);
			}
			
		}
		
		return errors;

		
		
	}

	private boolean duplicateEmailExists(String emailAddress,Integer userId) {
		
		logger.info("UserDataValidator, duplicateEmailExists, Email: " + emailAddress);
		boolean duplicateFound = false;
		try{
			//Email email = emailDao.getEmailByAddress(emailAddress);
			User user = userDao.getUserByEmailAddress(emailAddress);
			if(userId == null){
				// Means other user already have email address.
				duplicateFound = true;
			} else if(user != null && user.getId() != null && user.getId().intValue() != userId.intValue()){
				duplicateFound = true;
			}
			
		}catch(DataNotFoundException dnfe){
			// Multiple entries found. Add error and return.
			logger.info("UserDataValidator, duplicateEmailExists, No entity for given email address");
		}catch(InvalidDataException ide){
			// Multiple entries found. Add error and return.
			logger.info("UserDataValidator, duplicateEmailExists, Found multiple entities for given email address");
			duplicateFound = true;
		}catch(BaseAPIException bae){
			// Multiple entries found. Add error and return.
			logger.error("UserDataValidator, duplicateEmailExists, BaseAPIException", bae);
		}
		logger.info("UserDataValidator, duplicateEmailExists, for Email: {}, exists: ", emailAddress, duplicateFound);
		return duplicateFound;
		
	}

}
