package com.contact.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.contact.dto.MessageType;
import com.contact.entity.BaseEntity;
import com.contact.exception.InternalServerException;
import com.contact.utility.MessageDetailsHelper;



/**
 * Abstract DAO class for all DAOs.  
 * @author White Cube Solutions
 *
 */
public abstract class AbstractDAO< T extends BaseEntity> {
	
	private static final Logger logger = LogManager.getLogger(AbstractDAO.class);
	
	@PersistenceContext
    protected EntityManager entityManager;

    public void persist(T entity) {
    	entity.setCreatedDate(new Date());
    	entityManager.persist(entity);
    }
    
    public void update(T entity) {
    	entity.setModifiedDate(new Date());
    	entityManager.merge(entity);
    }
 
    public void delete(T entity) {
    	entityManager.remove(entity);
    }
    
    protected List<T> getListFromQuery(Query query) throws InternalServerException{
		logger.info("AbstractDAO, getListFromQuery, starts." );
		List<T> allObjects = null;
		try{
			allObjects = (List<T>) query.getResultList();
		} catch (Exception ex){
			logger.error("AbstractDAO, getListFromQuery, Exception ", ex);
			InternalServerException ise = new InternalServerException();
			String errorCode = MessageDetailsHelper.GENERAL_SERVER_ERR;
			String errorMessage = MessageDetailsHelper.getMessage(errorCode);
			ise.addMessage(errorCode, errorMessage, MessageType.Error);
			throw ise;
		}

		logger.info("AbstractDAO, getListFromQuery, ends." );
		return allObjects;
	}
    
    
    
	/**
	 * Get count from count query.
	 * 
	 * @param queryStr
	 * @return
	 */
	public int getCountFromQueryString(String queryStr){
		Query query = entityManager.createNamedQuery(queryStr);
		return getCountFromQuery(query);
	}
	
	public int getCountFromQuery(Query query){
		int count = ((Number) query.getSingleResult()).intValue();
		
		return count;
	}
	
}
