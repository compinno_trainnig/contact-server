package com.contact.dao;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;

import com.contact.dto.MessageType;
import com.contact.entity.User;
import com.contact.exception.DataNotFoundException;
import com.contact.exception.InternalServerException;
import com.contact.exception.InvalidDataException;
import com.contact.utility.MessageDetailsHelper;
import com.contact.utility.NamedQueryName;

@Configuration
public class UserDAO{
	private static final Logger logger = LogManager.getLogger(UserDAO.class);
	/**
	 * Method to save Users data to database. 
	 * @param org
	 * @return
	 */
	
	@PersistenceContext
    protected EntityManager entityManager;

		public User createUser(User user){
			logger.info("UserDAO, createUser, starts ");
			persist(user);
			logger.info("UserDAO, createUser, ends successfully. ");
			return user;
		}
		
		
		public void persist(User entity) {
	    	entityManager.persist(entity);
	    }
		
		 public void update(User entity) {
		    	
		    	entityManager.merge(entity);
		    }
		 


		public User getUserByEmailAddress(String emailAddr) throws DataNotFoundException, InvalidDataException, InternalServerException {
			
			logger.info("UserDAO, getUserByEmailAddress, starts for email = " + emailAddr);
			User user = null;
			
			if(emailAddr != null){
				Query query = entityManager.createNamedQuery(NamedQueryName.USER_FIND_BY_EMAIL);
				query.setParameter("emailAddr", emailAddr);
				try{
					user = (User) query.getSingleResult();
				} catch (NoResultException nre){
					logger.error("UserDAO, getUserByEmailAddress, NoResultException ", nre);
					DataNotFoundException dnf = new DataNotFoundException();
					String errorCode = MessageDetailsHelper.USER_NOT_FOUND;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					dnf.addMessage(errorCode, errorMessage, MessageType.Error);
					throw dnf;
				} catch (NonUniqueResultException nre){
					logger.error("UserDAO, getUserByEmailAddress, NonUniqueResultException ", nre);
					InvalidDataException inf = new InvalidDataException();
					String errorCode = MessageDetailsHelper.LOGIN_NOT_RECOGNIZED;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					inf.addMessage(errorCode, errorMessage, MessageType.Error);
					throw inf;
				} catch (Exception ex){
					logger.error("UserDAO, getUserByEmailAddress, Exception ", ex);
					InternalServerException ise = new InternalServerException();
					String errorCode = MessageDetailsHelper.GENERAL_SERVER_ERR;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					ise.addMessage(errorCode, errorMessage, MessageType.Error);
					throw ise;
				}

			} else {
				logger.error("UserDAO, getUserByEmailAddress, email is null. Throwing exception. ");
				InvalidDataException ide = new InvalidDataException();
				String errorCode = MessageDetailsHelper.VAL_INVALID_DATA_GENERIC;
				String errorMessage = MessageDetailsHelper.getMessage(errorCode);
				ide.addMessage(errorCode, errorMessage, MessageType.Error);
				throw ide;
			}
			logger.info("UserDAO, getUserByEmailAddress, ends with data = " + user);
			return user;
			
		}


		public User getUserById(Integer userId) throws DataNotFoundException, InternalServerException, InvalidDataException{
			logger.info("UserDAO, getUserById, starts for Id = " + userId);
			User user = null;
			
			if(userId != null){
				try{
					user = (User) entityManager.find(User.class, userId);
				} catch (Exception ex){
					logger.error("UserDAO, getUserById, Exception ", ex);
					InternalServerException ise = new InternalServerException();
					String errorCode = MessageDetailsHelper.GENERAL_SERVER_ERR;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					ise.addMessage(errorCode, errorMessage, MessageType.Error);
					throw ise;
				}
				
				if(user == null){
					logger.error("UserDAO, getUserById, No user found for id = " + userId);
					DataNotFoundException dnf = new DataNotFoundException();
					String errorCode = MessageDetailsHelper.USER_NOT_FOUND;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					dnf.addMessage(errorCode, errorMessage, MessageType.Error);
					throw dnf;
				}

			} else {
				logger.error("UserDAO, getUserById, userId is null. Throwing exception. ");
				InvalidDataException ide = new InvalidDataException();
				String errorCode = MessageDetailsHelper.VAL_INVALID_DATA_GENERIC;
				String errorMessage = MessageDetailsHelper.getMessage(errorCode);
				ide.addMessage(errorCode, errorMessage, MessageType.Error);
				throw ide;
			}
			logger.info("UserDAO, getUserById, ends with data = " + user);
			return user;
			
		}


		public User updateUser(User user) {
			logger.info("UserDAO, updateUser, starts ");
			update(user);
			logger.info("UserDAO, updateUser, ends successfully. ");
			return user;
			
		}
	    
		
		

	

}
