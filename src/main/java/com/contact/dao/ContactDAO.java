package com.contact.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;

import com.contact.dto.MessageType;
import com.contact.entity.Contact;
import com.contact.exception.DataNotFoundException;
import com.contact.exception.InternalServerException;
import com.contact.exception.InvalidDataException;
import com.contact.utility.MessageDetailsHelper;
import com.contact.utility.NamedQueryName;


@Configuration
public class ContactDAO {
	
	private static final Logger logger = LogManager.getLogger(ContactDAO.class);

	
	@PersistenceContext
    protected EntityManager entityManager;

	public Contact createMessage(Contact contact) {
		
		logger.info("ContactDAO, createContactDAO, starts ");
		persist(contact);
		logger.info("ContactDAO, createContactDAO, ends successfully. ");
		return contact;
		// TODO Auto-generated method stub
		
	}

	private void persist(Contact contact) {
		// TODO Auto-generated method stub
		entityManager.persist(contact);
		
	}
	public void delete(Contact contact) {
    	entityManager.remove(contact);
    }
	
	 

	public List<Contact> getAllCOntact(Integer userId) throws InvalidDataException {
		
		logger.info("ContactDAO, getContactDAOById, starts for id = ");
		List<Contact> contact = null;
		boolean success = true;
		
		if(success){
			Query query = entityManager.createNamedQuery(NamedQueryName.CONTACT);
			query.setParameter("userId", userId);
			contact =  getListFromQuery(query);
			
		} else {
			logger.error("ContactDAO, getContactDAO, ContactDAOID is null. Throwing exception. ");
			InvalidDataException ide = new InvalidDataException();
			String errorCode = MessageDetailsHelper.VAL_INVALID_DATA_GENERIC;
			String errorMessage = MessageDetailsHelper.getMessage(errorCode);
			ide.addMessage(errorCode, errorMessage, MessageType.Error);
			throw ide;
		}
		
		return contact;


	
	}

		
		private List<Contact> getListFromQuery(Query query) {
			List<Contact> allObjects = null;
			
			allObjects=(List<Contact>)query.getResultList();
			
			return allObjects;
		}

		public Contact getContactById(Integer broadcastMessageId) throws DataNotFoundException, InternalServerException, InvalidDataException{
			
			logger.info("ContactDAO, getContactDAOById, starts for id = " + broadcastMessageId);
			Contact contact = null;
			
			if(broadcastMessageId != null){
				try{
					Query query = entityManager.createNamedQuery(NamedQueryName.CONTACT_BY_ID);
					query.setParameter("contactID", broadcastMessageId);
					contact =  (Contact)query.getSingleResult();
					
				}  catch (NoResultException nre){
					logger.error("ContactDAO, getContactDAOById, NoResultException ");
					DataNotFoundException dnf = new DataNotFoundException();
					String errorCode = MessageDetailsHelper.ENTITY_NOT_FOUND;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode, "ContactDAO");
					dnf.addMessage(errorCode, errorMessage, MessageType.Error);
					throw dnf;
				} catch (NonUniqueResultException nre){
					logger.error("ContactDAO, getContactDAOById, NonUniqueResultException ");
					InvalidDataException inf = new InvalidDataException();
					String errorCode = MessageDetailsHelper.GENERAL_SERVER_ERR;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					inf.addMessage(errorCode, errorMessage, MessageType.Error);
					throw inf;
				}catch (PersistenceException ex){
					logger.error("ContactDAO, getContactDAOById, PersistenceException ", ex);
					InternalServerException ise = new InternalServerException();
					String errorCode = MessageDetailsHelper.GENERAL_SERVER_ERR;
					String errorMessage = MessageDetailsHelper.getMessage(errorCode);
					ise.addMessage(errorCode, errorMessage, MessageType.Error);
					throw ise;
				}
				
			} else {
				logger.error("ContactDAO, getContactDAO, ContactDAO is null. Throwing exception. ");
				InvalidDataException ide = new InvalidDataException();
				String errorCode = MessageDetailsHelper.VAL_INVALID_DATA_GENERIC;
				String errorMessage = MessageDetailsHelper.getMessage(errorCode);
				ide.addMessage(errorCode, errorMessage, MessageType.Error);
				throw ide;
			}
			
			return contact;
			
			
		}

		public void deleteBroadcastMessage(Contact contactEntity) {
			logger.info("ContactDAO, deleteContactDAO, starts:  = {}", contactEntity.getId());
			delete(contactEntity);


			
		}

		public void update(Contact contactEntity) {
			entityManager.merge(contactEntity);
			
		}

		public void deleteAllContact() {
			 int deletedCount = entityManager.createQuery("DELETE FROM Contact").executeUpdate();
			 logger.info("Delete all ends",deletedCount );
			


		
		}


	

}
