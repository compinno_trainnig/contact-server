package com.contact.security;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.contact.utility.MD5Util;



public class MD5HashPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		String strPassword = ""+ rawPassword;
		return MD5Util.hashString(strPassword);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		if(encodedPassword.equalsIgnoreCase(encode(rawPassword))){
			return true;
		}
		return false;
	}

}
