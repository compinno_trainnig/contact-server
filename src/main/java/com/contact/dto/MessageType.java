package com.contact.dto;

/**
 * Enum specifying type of message
 * @author Vinit
 *
 */
public enum MessageType {
	Success, Warning, Error;
}
