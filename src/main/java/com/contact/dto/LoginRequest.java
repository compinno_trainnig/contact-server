package com.contact.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class to be used as request to validate user's login.
 * 
 * @author Vinit
 *
 */
@JsonInclude(value=Include.NON_NULL)
public class LoginRequest {

	private String email;
	
	private String password;


	/**
	 * @return the email
	 */
	


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "password");
	}


}
