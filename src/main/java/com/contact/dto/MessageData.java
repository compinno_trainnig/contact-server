package com.contact.dto;

import com.contact.utility.MessageDetailsHelper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * Class specifying a Message. It can be Success message, Warning or Error.
 * 
 * @author Vinit
 *
 */
@JsonInclude(value=Include.NON_NULL)
public class MessageData {

	private String code;
	
	private String message;
	
	private MessageType type;

	public MessageData() {
		super();
	}
	
	public MessageData(String code, String message, MessageType type) {
		this.code = code;
		if(message != null){
			this.message = message;
		} else {
			this.message = MessageDetailsHelper.getMessage(code);
		}
		this.type = type;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the type
	 */
	public MessageType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(MessageType type) {
		this.type = type;
	}

}
