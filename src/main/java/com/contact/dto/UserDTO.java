package com.contact.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDTO {
	

	private Integer id;
	private String name;
	private String password;
	private String email;
	private String textPassword;
	
	public String getTextPassword() {
		return textPassword;
	}
	public void setTextPassword(String textPassword) {
		this.textPassword = textPassword;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	

	
	
	

	
	
	
	
	
	

}
