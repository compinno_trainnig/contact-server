package com.contact.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value=Include.NON_NULL)
public class BaseRestResponse {
	
	protected List<MessageData> messages = new ArrayList<>();

	private Object data;
	
	
	/**
	 * @return the messages
	 */
	public List<MessageData> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<MessageData> messages) {
		this.messages = messages;
	}

	/**
	 * Method to check if messages has error.
	 * @return
	 */
	public boolean hasError(){
		return hasMessageOfType(MessageType.Error);
	}
	
	/**
	 * Method to check if messages has warning.
	 * @return
	 */
	public boolean hasWarning(){
		return hasMessageOfType(MessageType.Warning);
	}
	
	/**
	 * Method to check if messages has success.
	 * @return
	 */
	public boolean hasSuccess(){
		return hasMessageOfType(MessageType.Success);
	}
	
	/**
	 * Util method to check if messages has a message of type.
	 * @param type
	 * @return
	 */
	private boolean hasMessageOfType(MessageType type ){
		boolean hasError = false;
		if(messages != null && !messages.isEmpty()){
			for (MessageData message : messages) {
				if(message.getType() != null && message.getType() == type){
					hasError = true;
					break;
				}
			}
		}
		
		return hasError;
	}

	
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	
	/**
	 * @return the accessInfo
	 */

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
} 
