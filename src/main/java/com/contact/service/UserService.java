package com.contact.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.contact.controller.api.UserAPI;

import com.contact.dao.UserDAO;
import com.contact.dto.BaseRestResponse;

import com.contact.dto.LoginRequest;
import com.contact.dto.MessageData;
import com.contact.dto.MessageType;
import com.contact.dto.UserDTO;
import com.contact.entity.User;
import com.contact.exception.BaseAPIException;
import com.contact.exception.DataNotFoundException;
import com.contact.exception.InternalServerException;
import com.contact.exception.InvalidDataException;
import com.contact.mapper.UserDataMapper;
import com.contact.utility.DataUtil;
import com.contact.utility.MD5Util;
import com.contact.utility.MessageDetailsHelper;
import com.contact.utility.RandomGenerator;
import com.contact.validator.UserDataValidator;



@Configuration
@Transactional(readOnly = true)
public class UserService {

	private static final Logger logger = LogManager.getLogger(UserAPI.class);

	@Autowired
	UserDAO userDao;

	@Autowired 
	EmailService emailService;
	
	@Autowired UserDataValidator userValidator;
	
	/**
	 * Service method to create user.
	 * 
	 * @param userdto
	 * @param errors
	 * @return
	 * @throws BaseAPIException
	 */
	@Transactional(readOnly = false, rollbackFor = { BaseAPIException.class })
	public UserDTO createUser(UserDTO userdto, List<MessageData> errors) throws BaseAPIException {

		logger.info("UserService, createUser, starts with data: " + userdto);
		errors.addAll(userValidator.validateCreateUserRequest(userdto));
		if (!DataUtil.isCollectionNullOrEmpty(errors)) {
			return userdto;
		}

		// Encrypt users password.
		User userEntity = UserDataMapper.mapDTOToEntity(userdto);
		String password = null;

		if (userEntity.getPassword() == null || userEntity.getPassword().length() == 0) {
			password = RandomGenerator.generate8CharRandomPassword();
			userEntity.setPassword(MD5Util.hashString(password));
			userdto.setTextPassword(password);
		}
		logger.info("Password is " + password);

		
		
		userDao.createUser(userEntity);
		userdto.setId(userEntity.getId());
		userdto.setPassword(null);

		
		String registeredEmail = userEntity.getEmail();
		String registeredPassword = userdto.getTextPassword();
		Map<String, String> dataMap = new HashMap<>();
		
		dataMap.put("userName", registeredEmail);
		dataMap.put("password",registeredPassword);
		emailService.sendWelcomeEmail(registeredEmail, dataMap);
		
		//userdto.setBranchDTO(userEntity.getBranch());
		
		
		
		
		

		logger.info("UserService, createUser, ends.");
		return userdto;
	}

	
	/**
	 * Service method to login user.
	 * 
	 * @param userdto
	 * @param errors
	 * @return
	 * @throws BaseAPIException
	 */
	public UserDTO validateLogin(LoginRequest request, List<MessageData> errors) {
		
		UserDTO userData = null;
		boolean emailAvailable = true;
		
		User userEntity = null;
		
		if(emailAvailable){
			try {
				userEntity = userDao.getUserByEmailAddress(request.getEmail());
			} catch (DataNotFoundException | InvalidDataException | InternalServerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// Validate using phone.
			//userEntity = userDao.getUserByPhoneNumber(loginData.getPhone().getNumber(), loginData.getPhone().getCountryCode());
		}
		
		if(DataUtil.isStringNotEmpty(request.getPassword()) && DataUtil.isStringNotEmpty(userEntity.getPassword())){
			
			String hashedPwd = MD5Util.hashString(request.getPassword());
			if(!userEntity.getPassword().equals(hashedPwd)){
				// Incorrect password.
				MessageData error = new MessageData(MessageDetailsHelper.INVALID_LOGIN_OR_PASSWORD, null, MessageType.Error);
				errors.add(error);
				return null;
			}
			userData = UserDataMapper.mapEntityToDTO(userEntity, true, true, true);
		} else {
			MessageData error = new MessageData(MessageDetailsHelper.INVALID_LOGIN_OR_PASSWORD, null, MessageType.Error);
			errors.add(error);
			return null;
		}
		//userData.setAuthenticated(true);
		
		
		
		
		
		
		
		
		// TODO Auto-generated method stub
		return userData;
	}
	
	
	
	
	/**
	 * Service method to update user password.
	 * 
	 * @param userdto
	 * @param errors
	 * @return
	 * @throws BaseAPIException
	 */
	@Transactional(readOnly = false, rollbackFor={BaseAPIException.class})
	public User updateUser(Integer userId, User user, List<MessageData> errors) throws BaseAPIException{
		logger.info("UserService, updateUser, starts, User Id = {}, Data = ", userId, user);
		
		if(user != null){
			User userEntity = userDao.getUserById(userId);
			user.setId(userId);
			if(DataUtil.isStringNotEmpty(user.getPassword())){
				userEntity.setPassword(MD5Util.hashString(user.getPassword()));
			}
			userDao.updateUser(userEntity);
		}
		
		return user;

		
		
	}

	
	/**
	 * Service method to update user.
	 * 
	 * @param userdto
	 * @param errors
	 * @return
	 * @throws BaseAPIException
	 */
	@Transactional(readOnly = false, rollbackFor={BaseAPIException.class})
	public UserDTO updateUserDetails(Integer userId, UserDTO userdto, List<MessageData> errors) throws BaseAPIException{
		logger.info("UserService, updateUser, starts, User Id = {}, Data = ", userId, userdto);
		User userEntity = null;
		if(userdto != null){
			
			userEntity = userDao.getUserById(userId);
			
			//UserDataValidator.

			
		}
		
		UserDataMapper.copymapDTOToEntity(userdto, userEntity);
		userDao.update(userEntity);
		userdto.setId(userId);
		
		return userdto;
	}
	

	
	
	
}
