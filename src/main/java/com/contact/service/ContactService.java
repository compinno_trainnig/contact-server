package com.contact.service;


import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import com.contact.dao.ContactDAO;
import com.contact.dao.UserDAO;
import com.contact.dto.ContactDTO;
import com.contact.dto.MessageData;
import com.contact.dto.MessageType;
import com.contact.entity.Contact;
import com.contact.entity.User;
import com.contact.exception.BaseAPIException;
import com.contact.exception.DataNotFoundException;
import com.contact.exception.InternalServerException;
import com.contact.exception.InvalidDataException;
import com.contact.mapper.ContactDataMapper;
import com.contact.utility.MessageDetailsHelper;


@Configuration
@Transactional(readOnly = true)
public class ContactService {	
	private static final Logger logger = LogManager.getLogger(ContactService.class);


	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	UserDAO userDao;
	
	

	
	@Transactional(readOnly = false, rollbackFor = { BaseAPIException.class })
	public ContactDTO createContact(Integer userID,ContactDTO contactdtos,List<MessageData> errors) throws InternalServerException, InvalidDataException {
		
		boolean success = true;
		
		User user = null;
		
		try {
			user = userDao.getUserById(userID); 
		}catch(DataNotFoundException dne){
			logger.error("OrganizationService, broadcastMessage, Org not found for id = " + userID);
			String errorCode = MessageDetailsHelper.VAL_INVALID_DATA;
			String message = MessageDetailsHelper.getMessage(errorCode, "Society");
			MessageData error = new MessageData(errorCode, message, MessageType.Error);
			errors.add(error);
			return null;
		}
		
		
		Contact contactEntity = ContactDataMapper.mapDTOToEntity(contactdtos);
		contactEntity.setUser(user);
		contactDAO.createMessage(contactEntity);
		return contactdtos;
		}




	public List<ContactDTO> getAllContact(Integer userId, List<MessageData> errors) {
		
		logger.info("BroadcastMessageService, getMessageById, starts, message Id = ");
		List<ContactDTO> broadcastMessageDTO = null;
		List<Contact> broadcastMessageEntity = null;
		try {
			broadcastMessageEntity = contactDAO.getAllCOntact(userId);
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		broadcastMessageDTO = ContactDataMapper.mapBroadcastMessageEntityToDto(broadcastMessageEntity);
		
		logger.info("BroadcastMessageService, getMessageById, ends, Message Data = "+broadcastMessageDTO);
		return broadcastMessageDTO;
		

	}




	public ContactDTO getMessageById(Integer messageId) {
		logger.info("BroadcastMessageService, getMessageById, starts, message Id = "+messageId);
		ContactDTO contactDTO = null;
		Contact contactEntity = null;
		try {
			contactEntity = contactDAO.getContactById(messageId);
		} catch (DataNotFoundException | InternalServerException | InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		contactDTO = ContactDataMapper.mapBroadcastMessageEntityToDto(contactEntity);
		
		logger.info("BroadcastMessageService, getMessageById, ends, Message Data = "+contactDTO);
		return contactDTO;
		

	}



	@Transactional(readOnly=false, rollbackFor=BaseAPIException.class)
	public boolean deleteContact(Integer contactId, List<MessageData> errors) throws DataNotFoundException, InternalServerException, InvalidDataException {
		logger.info("BroadcastMessageAPI, deleteBroadcastMessage, starts with messageId = {}", contactId);
		boolean success = true;
		
		if(contactId == null){
			String errorMsg = MessageDetailsHelper.getMessage(MessageDetailsHelper.VAL_INVALID_DATA, "message id");
			MessageData error = new MessageData(MessageDetailsHelper.VAL_INVALID_DATA, errorMsg, MessageType.Error);
			errors.add(error);
			return false;
		} else {
			Contact contactEntity = contactDAO.getContactById(contactId);
			
			contactDAO.deleteBroadcastMessage(contactEntity);
		}
		
		logger.info("BroadcastMessageAPI, deleteBroadcastMessage, ends success = {}", success);
		return success;
	}



	@Transactional(readOnly=false, rollbackFor={BaseAPIException.class})
	public ContactDTO updateBroadcastMessage(Integer contactId, ContactDTO contactDTO, List<MessageData> errors) throws BaseAPIException{
		logger.info("BroadcastMessageAPI, updateBroadcastMessage, starts with data = " + contactDTO);

		if(errors == null){
			errors = new ArrayList<>();
		}
		Contact contactEntity = null;
		
		try{
			if(contactId == null){
				String errorCode = MessageDetailsHelper.VAL_DATA_REQUIRED;
				String message = MessageDetailsHelper.getMessage(errorCode);
				MessageData error = new MessageData(errorCode, message, MessageType.Error);
				errors.add(error);
				return contactDTO;
			} else {
				contactEntity = contactDAO.getContactById(contactId);
			}
		}catch(DataNotFoundException | InternalServerException | InvalidDataException dne){
			logger.error("BroadcastMessageService, updateBroadcastMessage, BroadcastMessage not found for id = " + contactId);
			String errorCode = MessageDetailsHelper.VAL_INVALID_DATA;
			String message = MessageDetailsHelper.getMessage(errorCode, "BroadcastMessage");
			MessageData error = new MessageData(errorCode, message, MessageType.Error);
			errors.add(error);
			return contactDTO;
		}
		ContactDataMapper.copyDTOToEntity(contactDTO, contactEntity);
		contactDAO.update(contactEntity);
		contactDTO.setId(contactId);
		logger.info("BroadcastMessageService, updateBroadcastMessage, ends. BroadcastMessage Id = " + contactDTO.getId());

		
		
		
		return contactDTO;
	}



	@Transactional(readOnly=false, rollbackFor={BaseAPIException.class})
	public boolean deletaAllContactDetails(List<MessageData> errors) {
		boolean success = true;
		
		contactDAO.deleteAllContact();
		return success;
	}




}
