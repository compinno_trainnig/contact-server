package com.contact.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.contact.utility.EmailTemplate;


@Configuration
public class EmailService {

	private static final Logger logger = LogManager.getLogger(EmailService.class);
	
	@Autowired EmailSender sender;
	
	
	
	
	/**
	 * Method to send welcome email.
	 * 
	 * @param to
	 * @param data
	 * @return
	 */
	public boolean sendWelcomeEmail(String to, Map<String, String> data){
		
		logger.info("EmailService, sendFeedbackEmail starts with: TO: {}, data:{}", to, data);
		boolean success = true;
		
		success = sendEmail(to, data, EmailTemplate.USER_WELCOME_EMAIL);
		
		logger.info("EmailService, sendFeedbackEmail ends");
		return success;
	}
	
	public boolean sendEmail(String to, Map<String, String> data, EmailTemplate template){
		String templateName = template.getTemplateName();
		String subject = template.getSubject();
		
		return sender.sendEmail(to, templateName, subject, data);
	}
	
	public boolean sendEmail(List<String> to, Map<String, String> data, EmailTemplate template){
		String templateName = template.getTemplateName();
		String subject = template.getSubject();
		
		return sender.sendEmail(to, templateName, subject, data);
	}
	
	
}
