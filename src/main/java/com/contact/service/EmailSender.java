package com.contact.service;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.contact.utility.DataUtil;



@Configuration
public class EmailSender {
	
	private static final Logger logger = LogManager.getLogger(EmailSender.class);
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	VelocityEngine velocityEngine;

	/**
	 * Method to send email.
	 * 
	 * @param to
	 * @param templateName
	 * @param subject
	 * @param data
	 * @return
	 */
	public boolean sendEmail(final String to, final String templateName,final String subject ,final Map<String, String> data ){
		ArrayList<String> toList = new ArrayList<>();
		toList.add(to);
		return sendEmail(toList, templateName, subject, data);
	}
	
	public boolean sendEmail(final List<String> to, final String templateName,final String subject ,final Map<String, String> data ){
		logger.info("EmailSender, sendEmail starts: to={}, templateName={}, subject={}, data={}", to, templateName,subject, data);
		boolean success = true;
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			

            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
                
                InternetAddress[] toAddrArray = getInternetAddrArray(to);
                
                message.setTo(toAddrArray);
                message.setFrom("smarthome.test@gmail.com");
                message.setSubject(subject);
                
                Template template = velocityEngine.getTemplate("./templates/" + templateName);

                // Generate context from data.
                VelocityContext velocityContext = new VelocityContext();
                velocityContext.put("newline", "\n");
                if(!DataUtil.isObjectNullOrEmpty(data)){
                	Set<String> keys = data.keySet();
                	for (String key : keys) {
                		velocityContext.put(key, data.get(key));
					}
                }
                
                StringWriter stringWriter = new StringWriter();
                
                template.merge(velocityContext, stringWriter);
                
                message.setText(stringWriter.toString(),true);
            }
        };
        mailSender.send(preparator);
        logger.info("EmailSender, sendEmail ends for template = " + templateName);
        return success;

	}
	
	private static InternetAddress[] getInternetAddrArray(List<String> addresses){
		InternetAddress[] inetAddrArray = null;
        if(!DataUtil.isCollectionNullOrEmpty(addresses)){
        	
        	List<InternetAddress> addrList = new ArrayList<InternetAddress>();
        	for (String address : addresses) {
        		try {
					addrList.add(new InternetAddress(address));
				} catch (AddressException e) {
					e.printStackTrace();
				}
			}
        	inetAddrArray = new InternetAddress[addrList.size()];
        	addrList.toArray(inetAddrArray);
        }
        return inetAddrArray;
	}
	
}
