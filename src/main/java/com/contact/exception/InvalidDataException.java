package com.contact.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.contact.dto.MessageData;

public class InvalidDataException extends BaseAPIException {

	public InvalidDataException() {
		super();
	}

	public InvalidDataException(List<MessageData> errors) {
		super();
		this.messages = errors;
	}
	
	public InvalidDataException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDataException(String message) {
		super(message);
	}

	public InvalidDataException(Throwable cause) {
		super(cause);
	}
	
	@Override
	public HttpStatus getHttpResponseCode() {
		return HttpStatus.BAD_REQUEST;
	}
}
