package com.contact.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.contact.dto.MessageData;
import com.contact.dto.MessageType;

public class BaseAPIException extends Exception {

	public BaseAPIException() {
		super();
	}

	public BaseAPIException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BaseAPIException(String message, Throwable cause) {
		super(message, cause);
	}

	public BaseAPIException(String message) {
		super(message);
	}

	public BaseAPIException(Throwable cause) {
		super(cause);
	}

//	protected HttpStatus httpResponseCode = HttpStatus.INTERNAL_SERVER_ERROR;

	/**
	 * @return the httpResponseCode
	 */
	public HttpStatus getHttpResponseCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}
	
	protected List<MessageData> messages;

	/**
	 * @return the messages
	 */
	public List<MessageData> getMessages() {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages(List<MessageData> messages) {
		this.messages = messages;
	}

	public void addMessage(String code, String message, MessageType type){
		MessageData msg = new MessageData(code, message, type);
		
		if(messages == null){
			messages = new ArrayList<>();
		}
		
		messages.add(msg);
	}
	
	public int getIntHttpResponseCode() {
		return getHttpResponseCode().value();
	}
}
