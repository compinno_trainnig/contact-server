package com.contact.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.contact.dto.MessageData;

public class DataNotFoundException extends BaseAPIException {

	public DataNotFoundException() {
		super();
	}

	public DataNotFoundException(List<MessageData> errors) {
		super();
		this.messages = errors;
	}
	
	public DataNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DataNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataNotFoundException(String message) {
		super(message);
	}

	public DataNotFoundException(Throwable cause) {
		super(cause);
	}

	@Override
	public HttpStatus getHttpResponseCode() {
		return HttpStatus.NOT_FOUND;
	}
}
