package com.contact.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.contact.dto.MessageData;

public class InternalServerException extends BaseAPIException {

	public InternalServerException() {
		super();
	}

	public InternalServerException(List<MessageData> errors) {
		super();
		this.messages = errors;
	}
	
	public InternalServerException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InternalServerException(String message, Throwable cause) {
		super(message, cause);
	}

	public InternalServerException(String message) {
		super(message);
	}

	public InternalServerException(Throwable cause) {
		super(cause);
	}

	@Override
	public HttpStatus getHttpResponseCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}
}
