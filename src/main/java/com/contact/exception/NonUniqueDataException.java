package com.contact.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.contact.dto.MessageData;

public class NonUniqueDataException extends BaseAPIException {

	public NonUniqueDataException() {
		super();
	}

	public NonUniqueDataException(List<MessageData> errors) {
		super();
		this.messages = errors;
	}
	
	public NonUniqueDataException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NonUniqueDataException(String message, Throwable cause) {
		super(message, cause);
	}

	public NonUniqueDataException(String message) {
		super(message);
	}

	public NonUniqueDataException(Throwable cause) {
		super(cause);
	}

	@Override
	public HttpStatus getHttpResponseCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}
}
