package com.contact.mapper;

import java.util.ArrayList;
import java.util.List;

import com.contact.dto.ContactDTO;

import com.contact.entity.Contact;

import com.contact.utility.DataUtil;

public class ContactDataMapper {

	public static Contact mapDTOToEntity(ContactDTO contactDto) {

		Contact contact = new Contact();
		contact.setId(contactDto.getId());
		contact.setName(contactDto.getName());
		contact.setNumber(contactDto.getNumber());
		return contact;
	}

	public static List<ContactDTO> mapBroadcastMessageEntityToDto(
			List<Contact> broadcastMessageEntity) {
		
		
		List<ContactDTO> dtos = null;
		
		if(!DataUtil.isCollectionNullOrEmpty(broadcastMessageEntity)) {
			dtos = new ArrayList<>();
			
			for(Contact entity: broadcastMessageEntity) {
				dtos.add(mapBroadcastMessageEntityToDto(entity));
					
			}
		}
		

		return dtos;
		
		
			}

	public static ContactDTO mapBroadcastMessageEntityToDto(Contact entity) {
		ContactDTO contactDTO=new ContactDTO();
		
		contactDTO.setId(entity.getId());
		contactDTO.setName(entity.getName());
		contactDTO.setNumber(entity.getNumber());		
		return contactDTO;
	}

	public static void copyDTOToEntity(ContactDTO contactDTO, Contact contactEntity) {
			
		contactEntity.setName(contactDTO.getName());
		contactEntity.setNumber(contactDTO.getNumber());
		
	}

}
