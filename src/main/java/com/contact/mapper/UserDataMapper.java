package com.contact.mapper;

import org.apache.poi.hpsf.Section;


import com.contact.dto.UserDTO;

import com.contact.entity.User;

public class UserDataMapper {

	public static User mapDTOToEntity(UserDTO userdto2) {
		
		User userEntity = null;
		if(userdto2 != null){
			userEntity = new User();
			userEntity.setId(userdto2.getId());
			userEntity.setName(userdto2.getName());
			userEntity.setEmail(userdto2.getEmail());
		}
		return userEntity;
		
		
	}

	public static UserDTO mapEntityToDTO(User userEntity, boolean getBranch, boolean getYear, boolean getSemester) {
		//logger.info("UserAPI, login, ends, response = {}", userEntity);
		UserDTO userdto = null;
		if(userEntity != null){
			userdto = new UserDTO();
			userdto.setId(userEntity.getId());
			userdto.setName(userEntity.getName());
			userdto.setEmail(userEntity.getEmail());
	}
		return userdto;
}

	public static void copymapDTOToEntity(UserDTO userdto, User userEntity) {

		userEntity.setName(userdto.getName());
		userEntity.setEmail(userdto.getEmail());
		
		
	}
}		
